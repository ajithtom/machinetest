//
//  ListViewController.swift
//  machinetest
//
//  Created by Ajith Tom Jacob on 06/10/16.
//  Copyright © 2016 Ajith Tom Jacob. All rights reserved.
//

import UIKit

class ListViewController: UIViewController {

    @IBOutlet weak var photoList: UITableView!
    var imagesArray:[String]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBarHidden = true
        fetchImages()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func fetchImages(){
        let imagesFolder = Utility.documentDirectory().URLByAppendingPathComponent("stitches")
        
        do {
            // Get the directory contents urls (including subfolders urls)
            let directoryContents = try NSFileManager.defaultManager().contentsOfDirectoryAtURL( imagesFolder, includingPropertiesForKeys: nil, options: [])
            print(directoryContents)
            
            // if you want to filter the directory contents you can do like this:
            let pngFiles = directoryContents.filter{ $0.pathExtension == "png" }
            
            imagesArray = pngFiles.flatMap({$0.URLByDeletingPathExtension?.lastPathComponent})
            if(imagesArray?.count <= 0)
            {
                Utility.showAlertWith("Empty list", msg: "Go to camera view and try out the stitching")
            }
            self.photoList.reloadData()
            
        } catch let error as NSError {
            print(error.localizedDescription)
            Utility.showAlertWith("Empty list", msg: "Go to camera view and try out the stitching")
        }
    }

}


extension ListViewController : UITableViewDelegate, UITableViewDataSource{
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        if(imagesArray != nil)
        {
            return (imagesArray?.count)!
        }
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("imageCell", forIndexPath: indexPath) as! ImageCell
        cell.selectionStyle = .None
        
        if(imagesArray?.count > indexPath.row)
        {
            let imagesFolder = Utility.documentDirectory().URLByAppendingPathComponent("stitches")
            
            let imgName = imagesArray![indexPath.row]
            
            let imageURL = imagesFolder.URLByAppendingPathComponent(imgName + ".png")
            
            cell.imgVw?.image = UIImage(contentsOfFile: imageURL.path!)
            
            cell.timeLbl.text = imgName
        }
        
        
        return cell
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        print("did select row: \(indexPath.row)")
        if(imagesArray?.count > indexPath.row)
        {
            let imgName = imagesArray![indexPath.row]

            let imageView = self.storyboard!.instantiateViewControllerWithIdentifier("imageView") as? ImageViewController
            imageView?.setImageName(imgName)
            self.navigationController?.pushViewController(imageView!, animated: true)
        }
        
        
    }
}
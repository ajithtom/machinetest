//
//  SettingsViewController.swift
//  machinetest
//
//  Created by Ajith Tom Jacob on 06/10/16.
//  Copyright © 2016 Ajith Tom Jacob. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func myResumeBtnClicked(sender: AnyObject) {
        let webView = self.storyboard!.instantiateViewControllerWithIdentifier("webView") as? WebViewController
        self.navigationController?.pushViewController(webView!, animated: true)
    }

    @IBAction func logoutBtnClicked(sender: AnyObject) {
        
        let alertController: UIAlertController = UIAlertController(title: "Logout?", message: "Do you really want to Logout?", preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title: "No", style: .Default, handler: nil)
        let yesAction = UIAlertAction(title: "Yes", style: .Default) { (action:UIAlertAction) in
            
            Utility.saveStringForKey("", key: keyUserEmail)
            
        if((self.parentViewController?.parentViewController?.parentViewController?.isKindOfClass(UINavigationController)) != nil)
            {
                let navController = Utility.getAppDelegate().window?.rootViewController as? UINavigationController
                let loginView = self.storyboard!.instantiateViewControllerWithIdentifier("loginView") as? LoginViewController
                navController?.setViewControllers([loginView!], animated: false)
            }
                      
        }
        alertController.addAction(defaultAction)
        alertController.addAction(yesAction)
        self.presentViewController(alertController, animated: true, completion: nil)
        
        
    }
}

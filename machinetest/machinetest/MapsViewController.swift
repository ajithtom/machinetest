//
//  MapsViewController.swift
//  machinetest
//
//  Created by Ajith Tom Jacob on 06/10/16.
//  Copyright © 2016 Ajith Tom Jacob. All rights reserved.
//

import UIKit
import MBProgressHUD
import SDWebImage

class MapsViewController: UIViewController {

    @IBOutlet weak var mapView: GMSMapView!
    let locationManager = CLLocationManager()
    var placesArray:[NSDictionary]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        mapView.delegate = self
        readJSON()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func readJSON(){
        if let path = NSBundle.mainBundle().pathForResource("places", ofType: "json")
        {
            do
            {
                let jsonData = try NSData(contentsOfFile: path, options: NSDataReadingOptions.DataReadingMappedIfSafe)
                do
                {
                    let jsonResult = try NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.MutableContainers) as? NSDictionary
                    placesArray = jsonResult!["places"] as? [NSDictionary]
                    loadMarkers()
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
        }
    }
    
    func loadMarkers(){
        
        var bounds:GMSCoordinateBounds = GMSCoordinateBounds()
        for (index,placeDict) in placesArray!.enumerate()
        {
            let lat = Double((placeDict["latitude"] as? String)!)
            let lon = Double((placeDict["longitude"] as? String)!)
            
            let position:CLLocationCoordinate2D = CLLocationCoordinate2DMake(lat!, lon!)
            let marker:GMSMarker = GMSMarker(position: position)
            marker.title = placeDict["title"] as? String
            marker.icon = UIImage(named: "markerPin")
            marker.map = mapView
            marker.accessibilityLabel = "\(index)"
            marker.infoWindowAnchor = CGPointMake(0.5, 0.2)
            
            bounds = bounds.includingCoordinate(marker.position)
        }
        
        mapView.animateWithCameraUpdate(GMSCameraUpdate.fitBounds(bounds))

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MapsViewController : CLLocationManagerDelegate{
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .AuthorizedWhenInUse {
            
            locationManager.startUpdatingLocation()
            
            mapView.myLocationEnabled = true
            mapView.settings.myLocationButton = true
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            
            //mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            
            locationManager.stopUpdatingLocation()
        }
        
    }
    }

// MARK: - GMSMapViewDelegate
extension MapsViewController: GMSMapViewDelegate {
    func mapView(mapView: GMSMapView, idleAtCameraPosition position: GMSCameraPosition) {
    }
    
    
    func mapView(mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {

        let index:Int! = Int(marker.accessibilityLabel!)
        let placeDict:NSDictionary = placesArray![index]
        // 2
       
        let callOutView = NSBundle.mainBundle().loadNibNamed("Callout", owner: self, options: nil)[0] as! Callout
        
        let imgUrl = NSURL(string: (placeDict["image"] as? String)!)
        callOutView.imgVw.sd_setImageWithURL(imgUrl, placeholderImage: placeHolderImage, options: SDWebImageOptions.RetryFailed) { (image:UIImage!, error:NSError!, type:SDImageCacheType, url:NSURL!) -> Void in
            if(error != nil){
                callOutView.imgVw.image = placeHolderImage
            }
            else
            {
                dispatch_async(dispatch_get_main_queue()) {
                    callOutView.imgVw.image = image
                }
            }
        }
        
        callOutView.infoLbl.text = placeDict["title"] as? String

        return callOutView
    }
}
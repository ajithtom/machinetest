//
//  WebViewController.swift
//  machinetest
//
//  Created by Ajith Tom Jacob on 06/10/16.
//  Copyright © 2016 Ajith Tom Jacob. All rights reserved.
//

import UIKit
import MBProgressHUD

class WebViewController: UIViewController {

    @IBOutlet weak var webVw: UIWebView!
    var progressHUD:MBProgressHUD?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if let pdf = NSBundle.mainBundle().URLForResource("resume", withExtension: "pdf", subdirectory: nil, localization: nil)  {
            progressHUD = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            progressHUD!.labelText = "Loading..."
            let req = NSURLRequest(URL: pdf)
            webVw.loadRequest(req)
        }
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBarHidden = false
    }

    override func viewDidLayoutSubviews() {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        self.webVw.reload()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension WebViewController : UIWebViewDelegate{
    func webViewDidFinishLoad(webView: UIWebView) {
        progressHUD!.hide(true)
    }
}
//
//  Utility.swift
//  machinetest
//
//  Created by Ajith Tom Jacob on 06/10/16.
//  Copyright © 2016 Ajith Tom Jacob. All rights reserved.

import UIKit

public let keyUserEmail = "userEmail"
public let kMinPassLimit = 8
public let kMaxPassLimit = 32
public let imageMaxRes = 600

let googleMapsApiKey = "AIzaSyC-NBhtPD1qLYULo1vu5U-j-gct_-gm9zY"

public let placeHolderImage = UIImage(named: "placeholder")

class Utility: NSObject {

    
    class var sharedInstance : Utility {
        struct Static {
            static let instance : Utility = Utility()
        }
        return Static.instance
    }
    
    class func showAlertWith(tit : String, msg : String)
    {
        let alertController: UIAlertController = UIAlertController(title: tit, message: msg, preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alertController.addAction(defaultAction)
        let myNavController = Utility.getAppDelegate().window?.rootViewController as? UINavigationController
        
        if(myNavController?.presentedViewController != nil)
        {
            myNavController?.presentedViewController!.presentViewController(alertController, animated: true, completion: nil)
        }
        else
        {
            myNavController?.presentViewController(alertController, animated: true, completion: nil)
        }
        
    }


    class func validateEmail(strEmail: String) -> Bool {
        //let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailRegex = "^[A-Z0-9a-z._%+-]+@[A-Z0-9a-z.-]+\\.[A-Za-z]{2,}$"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        
        return emailTest.evaluateWithObject(strEmail)
        
    }
    
    class func saveStringForKey(object : String, key : String)
    {
        NSUserDefaults.standardUserDefaults().setObject(object, forKey: key)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    class func getBoolValueFromSettingsForKey(key : String) -> Bool
    {
        let defaults = NSUserDefaults.standardUserDefaults()
        let status = defaults.boolForKey(key)
        if status == true
        {
            return status
        }
        return false
    }
    
    class func saveBoolValueFromSettingsForKey(key : String, value : Bool)
    {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setValue(NSNumber(bool: value), forKey: key)
        userDefaults.synchronize()
    }
    
    class func getStringForKey(key : String) -> String
    {
        if let object = NSUserDefaults.standardUserDefaults().objectForKey(key)
        {
            return object as! String
        }
        return ""
    }
    
    class func getWidthOfLabelFromString(postTitle:String, font:UIFont, height:CGFloat) -> CGFloat {
        let constraintSize = CGSizeMake(CGFloat.max, height)
        let attributes = [NSFontAttributeName: font]
        let labelSize = postTitle.boundingRectWithSize(constraintSize,
                                                       options: NSStringDrawingOptions.UsesLineFragmentOrigin,
                                                       attributes: attributes,
                                                       context: nil)
        return labelSize.width
    }
    
    class func getHeightForTitle(postTitle: NSString, font:UIFont, width: CGFloat) -> CGFloat
    {
        let constraintSize = CGSizeMake(width, CGFloat.max)
        let attributes = [NSFontAttributeName: font]
        let labelSize = postTitle.boundingRectWithSize(constraintSize,
                                                       options: NSStringDrawingOptions.UsesLineFragmentOrigin,
                                                       attributes: attributes,
                                                       context: nil)
        let len = postTitle.rangeOfString("\n")
        let size = "y".boundingRectWithSize(constraintSize,
                                            options: NSStringDrawingOptions.UsesLineFragmentOrigin,
                                            attributes: attributes,
                                            context: nil)
        let extralines = size.height * CGFloat(len.length)
        return labelSize.height + extralines
    }
    
    class func documentDirectory() -> NSURL{
        let fileManager = NSFileManager.defaultManager()
        
        let docDir:NSURL = (fileManager.URLsForDirectory(NSSearchPathDirectory.DocumentDirectory, inDomains: NSSearchPathDomainMask.UserDomainMask)).first!
        
        return docDir
    }
    
    class func cacheDirectory() -> NSURL{
        let fileManager = NSFileManager.defaultManager()
        
        let cacheDir:NSURL = (fileManager.URLsForDirectory(NSSearchPathDirectory.CachesDirectory, inDomains: NSSearchPathDomainMask.UserDomainMask)).first!
        
        return cacheDir
    }
    
    class func getStoryBoardName() -> String {
        
//        if UIDevice().userInterfaceIdiom == .Pad
//        {
//            return "Main_iPad"
//        }
        return "Main"
    }
    
    class func getStoryBoard() -> UIStoryboard {
        return UIStoryboard(name: Utility.getStoryBoardName(), bundle: nil)
    }
    
    class func getAppDelegate() -> AppDelegate{
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }
    
    class func isIpad() -> Bool
    {
        if UIDevice().userInterfaceIdiom == .Pad
        {
            return true
        }
        return false
        
    }
    
    class func windowHeight() -> CGFloat{
        return UIScreen.mainScreen().bounds.size.height
    }
    
    class func windowWidth() -> CGFloat{
        return UIScreen.mainScreen().bounds.size.width
    }
    
    
    class func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight))
        image.drawInRect(CGRectMake(0, 0, newWidth, newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
}

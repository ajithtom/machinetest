//
//  LoginViewController.swift
//  machinetest
//
//  Created by Ajith Tom Jacob on 06/10/16.
//  Copyright © 2016 Ajith Tom Jacob. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var emailFld: UITextField!
    @IBOutlet weak var passFld: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.navigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        registerForKeyboardNotifications()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        deregisterFromKeyboardNotifications()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func registerForKeyboardNotifications(){
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LoginViewController.keyboardWillBeHidden(_:)), name: UIKeyboardWillHideNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LoginViewController.keyboardWillChangeFrame(_:)), name: UIKeyboardWillChangeFrameNotification, object: nil)
    }
    
    func deregisterFromKeyboardNotifications(){
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillChangeFrameNotification, object: nil)
    }
    
    func keyboardWillBeHidden(notification : NSNotification){
        var viewFrame = view.frame
        viewFrame.origin.y = 0
        view.frame = viewFrame
    }
    
    func keyboardWillChangeFrame(notification : NSNotification){
        let info = notification.userInfo
        let keyboardFrame = (info![UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        var viewFrame = view.frame
        viewFrame.origin.y = -(keyboardFrame.size.height/2)
        view.frame = viewFrame
    }
    
    @IBAction func loginBtnAction(sender: AnyObject) {
        emailFld.resignFirstResponder()
        passFld.resignFirstResponder()
        if(emailFld.text?.characters.count<=0)
        {
            Utility.showAlertWith("Email missing", msg: "Please enter your Email")
        }
        else if(!Utility.validateEmail(emailFld.text!))
        {
            Utility.showAlertWith("Invalid Email", msg: "Please enter a valid Email")
        }
        else if(passFld.text?.characters.count<=0)
        {
            Utility.showAlertWith("Password missing", msg: "Please enter your Password")
        }
        else if(passFld.text?.characters.count<kMinPassLimit)
        {
            Utility.showAlertWith("Password too short", msg: "Minimum \(kMinPassLimit) characters")
        }
        else if(passFld.text?.characters.count>kMaxPassLimit)
        {
            Utility.showAlertWith("Password too long", msg: "Maximum \(kMaxPassLimit) characters")
        }
        else
        {
            Utility.saveStringForKey((emailFld.text ?? ""), key: keyUserEmail)
            loadApp()
        }
    }
    
    func loadApp(){
        
        let tabBar = self.storyboard!.instantiateViewControllerWithIdentifier("tabBar")
        self.navigationController?.pushViewController(tabBar, animated: true)
    }

}

extension LoginViewController: UITextFieldDelegate{
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}

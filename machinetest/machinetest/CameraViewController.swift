//
//  CameraViewController.swift
//  machinetest
//
//  Created by Ajith Tom Jacob on 06/10/16.
//  Copyright © 2016 Ajith Tom Jacob. All rights reserved.
//

import UIKit
import AVFoundation

class CameraViewController: UIViewController {

    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var captureBtn: UIButton!
    var photoCount:Int = 0
    var counter:Int = 5
    
    var captureSession: AVCaptureSession?
    var stillImageOutput: AVCaptureStillImageOutput?
    var previewLayer: AVCaptureVideoPreviewLayer?
    @IBOutlet weak var photoCountLbl: UILabel!
    
    var image1:UIImage?
    var image2:UIImage?
    var image3:UIImage?
    var image4:UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        captureBtn.layer.borderColor = UIColor.whiteColor().CGColor
        photoCountLbl.layer.borderColor = UIColor.whiteColor().CGColor
        
        captureSession = AVCaptureSession()
        captureSession!.sessionPreset = AVCaptureSessionPresetPhoto
        
        prepareCameraFeed()
    
    }
    
    func prepareCameraFeed(){
        
        let backCamera = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        
        var error: NSError?
        var input: AVCaptureDeviceInput!
        do {
            input = try AVCaptureDeviceInput(device: backCamera)
            
            if error == nil && captureSession!.canAddInput(input) {
                captureSession!.addInput(input)
                stillImageOutput = AVCaptureStillImageOutput()
                stillImageOutput?.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
                
                if captureSession!.canAddOutput(stillImageOutput) {
                    captureSession!.addOutput(stillImageOutput)
                    // ...
                    // Configure the Live Preview here...
                    previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
                    previewLayer!.videoGravity = AVLayerVideoGravityResizeAspect
                    previewLayer!.connection?.videoOrientation = AVCaptureVideoOrientation.Portrait
                    previewView.layer.addSublayer(previewLayer!)
                    captureSession!.startRunning()
                    
                    
                }
            }
            
            
        } catch let error1 as NSError {
            error = error1
            input = nil
            print(error!.localizedDescription)
        }
    }

    override func viewDidLayoutSubviews() {
        if(self.interfaceOrientation == .Portrait)
        {
            previewLayer!.connection?.videoOrientation = AVCaptureVideoOrientation.Portrait
        }
        else if(self.interfaceOrientation == .PortraitUpsideDown)
        {
            previewLayer!.connection?.videoOrientation = AVCaptureVideoOrientation.PortraitUpsideDown
        }
        else if(self.interfaceOrientation == .LandscapeLeft)
        {
            previewLayer!.connection?.videoOrientation = AVCaptureVideoOrientation.LandscapeLeft
        }
        else
        {
            previewLayer!.connection?.videoOrientation = AVCaptureVideoOrientation.LandscapeRight
        }
        previewLayer!.frame = CGRectMake(0, 0, previewView.frame.size.width, previewView.frame.size.height)
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func captureClicked(sender: AnyObject) {
        captureBtn.enabled = false
        capturePicture()
    }
    
    
    func capturePicture(){
        self.captureBtn.setTitle("5", forState: .Normal)
        if let videoConnection = stillImageOutput!.connectionWithMediaType(AVMediaTypeVideo) {
            // ...
            stillImageOutput?.captureStillImageAsynchronouslyFromConnection(videoConnection, completionHandler: { (sampleBuffer, error) -> Void in
                // ...
                self.getImageFromBuffer(sampleBuffer)
            })
        }
    }
    
    
    func timer(){
        if(counter == 1)
        {
            counter = 5
            self.captureBtn.setTitle("1", forState: .Normal)
            capturePicture()
        }
        else
        {
            counter = counter - 1
            self.captureBtn.setTitle("\(counter ?? 0)", forState: .Normal)
            self.performSelector(#selector(CameraViewController.timer), withObject: nil, afterDelay: 1.0)
        }
    }
    
    func getImageFromBuffer(sampleBuffer : CMSampleBuffer){
        let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(sampleBuffer)
        let dataProvider = CGDataProviderCreateWithCFData(imageData)
        let cgImageRef = CGImageCreateWithJPEGDataProvider(dataProvider, nil, true, CGColorRenderingIntent.RenderingIntentDefault)
        let image = UIImage(CGImage: cgImageRef!, scale: 1.0, orientation: UIImageOrientation.Right)
        if(photoCount == 0)
        {
            image1 = image
            photoCount = 1
        }
        else if(photoCount == 1)
        {
            image2 = image
            photoCount = 2
        }
        else if(photoCount == 2)
        {
            image3 = image
            photoCount = 3
        }
        else if(photoCount == 3)
        {
            image4 = image
            photoCount = 4
        }
        photoCountLbl.text = "\(photoCount ?? 0) / 4"
        if(photoCount == 4)
        {
            processAndSave()
        }
        else
        {
            self.performSelector(#selector(CameraViewController.timer), withObject: nil, afterDelay: 1.0)
        }
    }
    
    
    func processAndSave(){

        let size = CGSize(width: imageMaxRes, height: imageMaxRes)
        UIGraphicsBeginImageContext(size)
        
        let imageSize1 = CGRect(x: 0, y: 0, width: imageMaxRes/2, height: imageMaxRes/2)
        image1!.drawInRect(imageSize1)
        
        let imageSize2 = CGRect(x: imageMaxRes/2, y: 0, width: imageMaxRes/2, height: imageMaxRes/2)
        image2!.drawInRect(imageSize2)
        
        let imageSize3 = CGRect(x: 0, y: imageMaxRes/2, width: imageMaxRes/2, height: imageMaxRes/2)
        image3!.drawInRect(imageSize3)
        
        let imageSize4 = CGRect(x: imageMaxRes/2, y: imageMaxRes/2, width: imageMaxRes/2, height: imageMaxRes/2)
        image4!.drawInRect(imageSize4)
        
        
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        image1 = nil
        image2 = nil
        image3 = nil
        image4 = nil
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy hh:mm:ss"
        
        let dateStr = dateFormatter.stringFromDate(NSDate())
        
        print("DAtestr : \(dateStr)")
        
        let imagesFolder = Utility.documentDirectory().URLByAppendingPathComponent("stitches")
        
        if(!NSFileManager.defaultManager().fileExistsAtPath(imagesFolder.path!))
        {
            do
            {
                try NSFileManager.defaultManager().createDirectoryAtURL(imagesFolder, withIntermediateDirectories: false, attributes: nil)
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        
        if let image:UIImage = newImage {
            if let data = UIImagePNGRepresentation(image) {
                let fileURL = imagesFolder.URLByAppendingPathComponent("\(dateStr).png")
                do
                {
                    try data.writeToURL(fileURL, options: NSDataWritingOptions.AtomicWrite)
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
        
        photoCount = 0
        photoCountLbl.text = "\(photoCount ?? 0) / 4"
        captureBtn.setTitle("Capture", forState: .Normal)
        captureBtn.enabled = true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  ImageViewController.swift
//  machinetest
//
//  Created by Ajith Tom Jacob on 07/10/16.
//  Copyright © 2016 Ajith Tom Jacob. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController {

    @IBOutlet weak var mainImage: UIImageView!
    var imgName:String?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if(imgName != nil)
        {
            let imagesFolder = Utility.documentDirectory().URLByAppendingPathComponent("stitches")
            
            
            let imageURL = imagesFolder.URLByAppendingPathComponent(imgName! + ".png")
            
            mainImage.image = UIImage(contentsOfFile: imageURL.path!)
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBarHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setImageName(imNm : String)
    {
        imgName = imNm
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ImageViewController : UIScrollViewDelegate{
    
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return mainImage
    }
}

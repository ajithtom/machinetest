//
//  ImageCell.swift
//  machinetest
//
//  Created by Ajith Tom Jacob on 07/10/16.
//  Copyright © 2016 Ajith Tom Jacob. All rights reserved.
//

import UIKit

class ImageCell: UITableViewCell {

    @IBOutlet weak var imgVw: UIImageView!
    @IBOutlet weak var timeLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.imgVw?.layer.cornerRadius = (self.imgVw?.frame.size.width)!/2
        
        self.imgVw?.clipsToBounds = true
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  ViewController.swift
//  machinetest
//
//  Created by Ajith Tom Jacob on 06/10/16.
//  Copyright © 2016 Ajith Tom Jacob. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.performSelector(#selector(ViewController.checkAndNavigate), withObject: nil, afterDelay: 2.0)
        self.navigationController?.navigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func checkAndNavigate(){
        let userEmail = Utility.getStringForKey(keyUserEmail)
        
        if(userEmail.characters.count > 0)
        {
            loadApp()
        }
        else
        {
            loadLogin()
        }
    }

    func loadApp(){
        
        let tabBar = self.storyboard!.instantiateViewControllerWithIdentifier("tabBar")
        self.navigationController?.pushViewController(tabBar, animated: true)
    }
    
    func loadLogin(){
        let loginView = self.storyboard!.instantiateViewControllerWithIdentifier("loginView") as? LoginViewController
        self.navigationController?.pushViewController(loginView!, animated: false)
    }
}

